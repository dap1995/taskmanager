/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.controller;

import java.io.IOException;
import javafx.scene.control.Alert;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import persistence.entities.Usuario;

/**
 *
 * @author dap
 */
public class SessionManager {
     private static SessionManager sessionManager = new SessionManager();

    
    private SessionManager(){
        
    }
    
    public static SessionManager getInstance(){
        return sessionManager;
    }
    
    public boolean isValid(HttpServletRequest request){
        boolean isValid=true;
        HttpSession session = request.getSession(false);
        if (session != null){
            if((request.getSession().getAttribute("userId") == null) || (session.getAttribute("userName") == null)){
                isValid=false;
            }
        } else {
            isValid=false;
        }
        return isValid;
        
    }
    
    public void doLogout(HttpServletRequest request, HttpServletResponse response, String msg) throws IOException{
        if (isValid(request)){
            destroy(request); 
           response.sendRedirect("Login.html");
        } else {
            response.sendRedirect("Login.html");
        }
    }
    
    public void create(HttpServletRequest request, Usuario usuario){
        request.getSession().setAttribute("userName", usuario.getUsername());
        request.getSession().setAttribute("passwd", usuario.getPassword());
        request.getSession().setAttribute("lastPage", "Login.html");
    }
    
    public void destroy(HttpServletRequest request){
        request.removeAttribute("userName");
        request.removeAttribute("passwd");
        request.removeAttribute("lastPage");
        request.getSession().invalidate();
    }
}
