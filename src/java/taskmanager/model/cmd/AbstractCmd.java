/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.model.cmd;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author dap
 */
public abstract class AbstractCmd {
    
    protected String readParameter(HttpServletRequest request,
            String parameterName, String defaultValue) {
        String theValue = request.getParameter(parameterName);
        if (theValue == null) {
            theValue = defaultValue;
        }
        
        return theValue;
    }
}
