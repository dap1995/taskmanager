/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.model.cmd;

import taskmanager.model.cmd.AbstractCmd;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.catolica.web.controller.interfaces.IWebCmd;
import persistence.daos.UsuarioJpaController;
import persistence.entities.Usuario;

/**
 *
 * @author dap
 */
public class UserEditCmd extends AbstractCmd implements IWebCmd {

    private static final String PERSISTENCE_UNIT_NAME = "tm-persistence";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = readParameter(request, "cmd", "");

        if (cmd.equals("UserEditCmd")) {
            String username = this.readParameter(request, "inputLogin", "");
            String name = this.readParameter(request, "inputName", "");
            String password = this.readParameter(request, "inputPassword", "");
            String id = this.readParameter(request, "id", "");
            
            try {
                EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
                UsuarioJpaController dao = new UsuarioJpaController(factory);
                Usuario usuario = dao.findUsuario(Long.parseLong(id));
                usuario.setName(name);
                usuario.setUsername(username);
                usuario.setPassword(password);
                dao.edit(usuario);
            } catch (Exception ex) {
                System.out.print("Excessão: " + ex.getStackTrace());
                request.setAttribute("error", ex.getMessage());
                request.setAttribute("back", "UserEditModelCmd");
                return "Error.jsp";
            }
            return "ControllerServlet?cmd=UserListModelCmd";
        }
        return "ControllerServlet?cmd=UserListModelCmd";
    }

}
