/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.model.cmd;

import java.io.IOException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.catolica.web.controller.interfaces.IWebCmd;
import persistence.daos.TasksJpaController;
import persistence.daos.UsuarioJpaController;
import persistence.entities.Tasks;
import persistence.entities.Usuario;

/**
 *
 * @author dap
 */
public class TasksCreateCmd extends AbstractCmd implements IWebCmd {
    private static final String PERSISTENCE_UNIT_NAME = "tm-persistence";

    
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = readParameter(request, "cmd", "");
        
        if (cmd.equals("TasksCreateCmd")) {
            String name = this.readParameter(request, "inputName", "");
            String description = this.readParameter(request, "inputDescription", "");
            
            try {
                EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
                TasksJpaController dao = new TasksJpaController(factory);
                Tasks task = new Tasks();
                task.setName(name);
                task.setDescription(description);
                //task.setCreator();
                //task.setTeam();
                dao.create(task);
            } catch (Exception ex) {
                System.out.print("Excessão: " + ex.getStackTrace());
                request.setAttribute("error", ex.getMessage());
                request.setAttribute("back", "TaskCreateModelCmd");
                return "Error.jsp";
            }
            return "ControllerServlet?cmd=TasksListModelCmd";
        }
        return "Login.html";
    }
    
}
