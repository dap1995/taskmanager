/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.model.cmd;

import taskmanager.model.cmd.AbstractCmd;
import java.io.IOException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.catolica.web.controller.interfaces.IAuthCmd;
import persistence.daos.UsuarioJpaController;
import persistence.entities.Usuario;

/**
 *
 * @author dap
 */
public class AuthenticationCmd extends AbstractCmd implements IAuthCmd {
    private static final String PERSISTENCE_UNIT_NAME = "tm-persistence";
    
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String cmd = readParameter(request, "cmd", "");
        
        if (cmd.equals("AuthenticationCmd")) {
            String login = this.readParameter(request, "inputLogin", "");
            String password = this.readParameter(request, "inputPassword", "");
            
            try {
                EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
                UsuarioJpaController dao = new UsuarioJpaController(factory);
                Usuario usuario;
                usuario = dao.findUsuario(login, password);
                if (usuario != null) {
                    HttpSession session = request.getSession(true);
                    session.setAttribute("userId", usuario.getId());
                    session.setAttribute("userName", usuario.getUsername());
                    session.setAttribute("Name", usuario.getName());
                    response.sendRedirect("ControllerServlet?cmd=UserListModelCmd");
                } else {
                    request.setAttribute("error", "Falha na autenticação");
                    request.setAttribute("back", "LoginRedirectCmd");
                    request.getServletContext().getRequestDispatcher("Error.jsp").forward(request, response);
                }
            } catch (Exception ex) {
                System.out.print("Excessão: " + ex.getStackTrace());
                request.setAttribute("error", "Falha na autenticação");
                request.setAttribute("back", "LoginRedirectCmd");
                response.sendRedirect("Error.jsp");
            }
        }
    }
    
    private static Usuario findUsuario(String username, String password){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        UsuarioJpaController dao = new UsuarioJpaController(factory);
        return dao.findUsuario(username, password);
    }
    
    
    
}
