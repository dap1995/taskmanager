/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.model.cmd;

import taskmanager.model.cmd.AbstractCmd;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.catolica.web.controller.interfaces.IWebCmd;
import persistence.daos.TeamJpaController;
import persistence.entities.Team;

/**
 *
 * @author dap
 */
public class TeamEditCmd extends AbstractCmd implements IWebCmd {

    private static final String PERSISTENCE_UNIT_NAME = "tm-persistence";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = readParameter(request, "cmd", "");

        if (cmd.equals("TeamEditCmd")) {
            String description = this.readParameter(request, "inputDescription", "");
            String name = this.readParameter(request, "inputName", "");
            String id = this.readParameter(request, "id", "");
            
            try {
                EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
                TeamJpaController dao = new TeamJpaController(factory);
                Team team = dao.findTeam(Long.parseLong(id));
                team.setName(name);
                team.setDescription(description);
                dao.edit(team);
            } catch (Exception ex) {
                System.out.print("Excessão: " + ex.getStackTrace());
                request.setAttribute("error", ex.getMessage());
                request.setAttribute("back", "TeamEditModelCmd");
                return "Error.jsp";
            }
            return "ControllerServlet?cmd=TeamListModelCmd";
        }
        return "ControllerServlet?cmd=TeamListModelCmd";
    }

}
