/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.model.cmd;

import taskmanager.model.cmd.AbstractCmd;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.catolica.web.controller.interfaces.IWebCmd;
import persistence.daos.UsuarioJpaController;
import persistence.entities.Usuario;

/**
 *
 * @author dap
 */
public class LoginRedirectCmd extends AbstractCmd implements IWebCmd {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = readParameter(request, "cmd", "");

        if (cmd.equals("LoginRedirectCmd")) {
            return "Login.html";
        }
        return "ControllerServlet?cmd=UserListModelCmd";
    }

}
