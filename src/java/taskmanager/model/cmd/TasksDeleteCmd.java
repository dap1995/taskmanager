/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.model.cmd;

import taskmanager.model.cmd.AbstractCmd;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.catolica.web.controller.interfaces.IWebCmd;
import persistence.daos.TasksJpaController;
import persistence.entities.Tasks;

/**
 *
 * @author dap
 */
public class TasksDeleteCmd extends AbstractCmd implements IWebCmd {

    private static final String PERSISTENCE_UNIT_NAME = "tm-persistence";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = readParameter(request, "cmd", "");

        if (cmd.equals("TasksDeleteCmd")) {
            String id = this.readParameter(request, "id", "");

            try {
                EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
                TasksJpaController dao = new TasksJpaController(factory);
                Tasks tasks = dao.findTasks(Long.parseLong(id));
                dao.destroy(tasks.getId());
            } catch (Exception ex) {
                System.out.print("Excessão: " + ex.getStackTrace());
                request.setAttribute("error", ex.getMessage().toString());
                request.setAttribute("back", "TasksListModelCmd");
                request.setAttribute("id", id);
                return "Error.jsp";
            }
            return "ControllerServlet?cmd=TasksListModelCmd";
        }
        return "ControllerServlet?cmd=TasksListModelCmd";
    }

}
