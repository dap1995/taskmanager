/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.model.cmd;

import taskmanager.model.cmd.AbstractCmd;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.catolica.web.controller.interfaces.IWebCmd;
import persistence.daos.TasksJpaController;
import persistence.daos.TasksJpaController;
import persistence.entities.Tasks;
import persistence.entities.Tasks;

/**
 *
 * @author dap
 */
public class TasksListModelCmd extends AbstractCmd implements IWebCmd {

    private static final String PERSISTENCE_UNIT_NAME = "tm-persistence";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("utf-8");
        /*
         String page = request.getParameter("page");
         int intPage;
         int totalPages = 1;
         int firstResult = 0;
         if ( page != null && (!page.equals("0") || !page.equals("1"))){
         intPage = Integer.parseInt(page);
         firstResult =  ((intPage * 10) + 1) -10;
         }*/
        try {
            EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
            TasksJpaController dao = new TasksJpaController(factory);
            List<Tasks> tasks = dao.findTasksEntities();
            //totalPages = (dao.getTasksCount()/10);
            request.setAttribute("tasks", tasks);
            request.setAttribute("entidade", tasks.get(0).getClass().getSimpleName());
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("tabela vazia");
            request.setAttribute("entidade", "Tasks");
        } catch (Exception ex) {
            System.out.print("Excessão: " + ex.getStackTrace());
            request.setAttribute("error", ex.getMessage());
            request.setAttribute("back", "LoginRedirectCmd");
            return "Error.jsp";
        }
        return "MainMenuTasks.jsp";
    }

}
