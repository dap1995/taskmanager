/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.model.cmd;

import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.catolica.web.controller.interfaces.IWebCmd;
import persistence.daos.TasksJpaController;
import persistence.entities.Tasks;

/**
 *
 * @author dap
 */
public class TasksEditModelCmd extends AbstractCmd implements IWebCmd {

    private static final String PERSISTENCE_UNIT_NAME = "tm-persistence";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("utf-8");
        
        String cmd = readParameter(request, "cmd", "");
        
        if (cmd.equals("TasksEditModelCmd")) {
            String id = this.readParameter(request, "id", "");

            try {
                EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
                TasksJpaController dao = new TasksJpaController(factory);
                Tasks tasks = dao.findTasks(Long.parseLong(id));
                request.setAttribute("objeto", tasks);
                request.setAttribute("entidade", tasks.getClass().getSimpleName());
                request.setAttribute("action", "Edit");
            } catch (Exception ex){
                System.out.print("Excessão: " + ex.getStackTrace());
                request.setAttribute("error", ex.getMessage());
                request.setAttribute("back", "TasksListModelCmd");
                return "Error.jsp";
            }
            return "EditTasks.jsp";
        }
        return "MainMenu.jsp";
    }
}
