/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.model.cmd;

import taskmanager.model.cmd.AbstractCmd;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.catolica.web.controller.interfaces.IWebCmd;
import persistence.daos.UsuarioJpaController;
import persistence.entities.Usuario;

/**
 *
 * @author dap
 */
public class UserCreateCmd extends AbstractCmd implements IWebCmd {

    private static final String PERSISTENCE_UNIT_NAME = "tm-persistence";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = readParameter(request, "cmd", "");

        if (cmd.equals("UserCreateCmd")) {
            String username = this.readParameter(request, "inputLogin", "");
            String name = this.readParameter(request, "inputName", "");
            String password = this.readParameter(request, "inputPassword", "");

            try {
                Usuario usuario = new Usuario();
                usuario.setName(name);
                usuario.setUsername(username);
                usuario.setPassword(password);
                EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
                UsuarioJpaController dao = new UsuarioJpaController(factory);
                dao.create(usuario);
            } catch (Exception ex) {
                System.out.print("Excessão: " + ex.getStackTrace());
                request.setAttribute("error", ex.getMessage());
                request.setAttribute("back", "UserCreateModelCmd");
                return "Error.jsp";
            }
            return "ControllerServlet?cmd=UserListModelCmd";
        }
        return "ControllerServlet?cmd=UserListModelCmd";
    }

}
