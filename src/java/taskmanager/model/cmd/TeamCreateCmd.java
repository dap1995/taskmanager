/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager.model.cmd;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.catolica.web.controller.interfaces.IWebCmd;
import persistence.daos.TasksJpaController;
import persistence.daos.TeamJpaController;
import persistence.daos.TeamJpaController;
import persistence.entities.Tasks;
import persistence.entities.Team;
import persistence.entities.Team;

/**
 *
 * @author dap
 */
public class TeamCreateCmd extends AbstractCmd implements IWebCmd {
    private static final String PERSISTENCE_UNIT_NAME = "tm-persistence";
    
    
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cmd = readParameter(request, "cmd", "");
        
        if (cmd.equals("TeamCreateCmd")) {
            String name = this.readParameter(request, "inputName", "");
            String description = this.readParameter(request, "inputDescription", "");
            
            try {
                EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
                TeamJpaController dao = new TeamJpaController(factory);
                Team team = new Team();
                team.setName(name);
                team.setDescription(description);
                dao.create(team);
            } catch (Exception ex) {
                System.out.print("Excessão: " + ex.getStackTrace());
                request.setAttribute("error", ex.getMessage());
                request.setAttribute("back", "TeamCreateModelCmd");
                return "Error.jsp";
            }
            return "ControllerServlet?cmd=TeamListModelCmd";
        }
        return "ControllerServlet?cmd=LoginRedirectCmd";
    }
    
    
}
