<%-- 
    Document   : ICreateUsuarios
    Created on : 03/12/2015, 01:48:58
    Author     : dap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div id="main" class="container-fluid">

            <h3 class="page-header">Criar ${entidade}</h3>

            <form action="ControllerServlet" method="post">
                <div class="row">
                    <input id="cmd" name="cmd" type="hidden" value="TeamCreateCmd">  
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Name</label>
                        <input id="inputName" name="inputName" type="text" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Description</label>
                        <input id="inputDescription" name="inputDescription" type="text" class="form-control" placeholder="Description">
                    </div>
                </div>

                <hr />

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <a href="ControllerServlet?cmd=TeamListModelCmd" class="btn btn-default">Cancelar</a>
                    </div>
                </div>

            </form>
        </div>

    </body>
</html>
