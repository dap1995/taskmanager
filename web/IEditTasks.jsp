<%-- 
    Document   : IEditUsuarios
    Created on : 02/12/2015, 23:00:02
    Author     : dap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
    <div id="main" class="container-fluid">
  
  <h3 class="page-header">${action} ${entidade}</h3>
  
  <form action="ControllerServlet" method="post">
  	<div class="row">
          <input id="cmd" name="cmd" type="hidden" value="TasksEditCmd"> 
          <input id="id" name="id" type="hidden" value=${objeto.id}> 
  	  <div class="form-group col-md-4">
  	  	<label for="exampleInputEmail1">Name</label>
  	  	<input id="inputName" name="inputName" type="text" class="form-control" value=${objeto.name}>
  	  </div>
	  <div class="form-group col-md-4">
  	  	<label for="exampleInputEmail1">Description</label>
  	  	<input id="inputDescription" name="inputDescription" type="text" class="form-control" value=${objeto.description}>
  	  </div>
	</div>
	
	<hr />
	
	<div class="row">
	  <div class="col-md-12">
	  	<button type="submit" class="btn btn-primary">Salvar</button>
		<a href="ControllerServlet?cmd=TasksListModelCmd" class="btn btn-default">Cancelar</a>
	  </div>
	</div>

  </form>
 </div>
 
    </body>
</html>
