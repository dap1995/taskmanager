<%-- 
    Document   : IDatagrid
    Created on : 22/11/2015, 00:23:25
    Author     : dap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="assets/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="assets/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
        <link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script>
            $().ready(function () {
                $('#delete-modal').on('show.bs.modal', function (e) {
                    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
                    $(this).find('.btn-ok').attr('entidade-id', $(e.relatedTarget).data('entidade-id'));
                    $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('entidade-id') + '</strong>');
                });
            });
        </script>
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalLabel">Excluir ${entidade}</h4>
                    </div>
                    <div class="modal-body">
                        Deseja mesmo Eliminar: ${entidade} - <p class="debug-url"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-danger btn-ok">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="main" class="container-fluid" style="margin-top: 50px">

            <div id="top" class="row">
                <div class="col-sm-3">
                    <h2>${entidade}</h2>
                </div>
                <div class="col-sm-6">

                    <div class="input-group h2">
                    </div>

                </div>
                <div class="col-sm-3">
                    <a href="ControllerServlet?cmd=TasksCreateModelCmd" class="btn btn-primary pull-right h2">New ${entidade}</a>
                </div>
            </div> <!-- /#top -->


            <hr />
            <div id="list" class="row">
                <div class="table-responsive col-md-12">
                    <table id="myTable" class="table table-striped" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Descrição</th>
                                <th class="actions">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="task" items="${tasks}">
                            <tr>
                                <td> ${task.id} </td>
                                <td> ${task.name} </td>
                                <td> ${task.description} </td>
                                <td class='actions'>
                                    <a id='view' class='btn btn-success btn-xs' href='ControllerServlet?cmd=TasksViewModelCmd&id=${task.id}'> Visualizar</a>
                                    <a id='edit' class='btn btn-warning btn-xs' href='ControllerServlet?cmd=TasksEditModelCmd&id=${task.id}'> Editar </a>
                                    <a id='delete' class='btn btn-danger btn-xs' data-toggle='modal' data-entidade-id="${task.id}" data-href='ControllerServlet?cmd=TasksDeleteCmd&id=${task.id}' href="#" data-target='#delete-modal'>Excluir</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div> <!-- /#list -->


        </div> <!-- /#main -->
    </body>
</html>
