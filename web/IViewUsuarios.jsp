<%-- 
    Document   : IView
    Created on : 02/12/2015, 22:16:44
    Author     : dap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${entidade} - View</title>
    </head>
    <body>
        <!-- Modal -->
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Excluir Item</h4>
      </div>
      <div class="modal-body">
        Deseja realmente excluir este ${entidade}?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Sim</button>
	<button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
      </div>
    </div>
  </div>
</div>

 <div id="main" class="container-fluid">
  <h3 class="page-header">Visualizar ${entidade} - ${objeto.id} </h3>
  
  <div class="row">
    <div class="col-md-4">
      <p><strong>Nome</strong></p>
  	  <p>${objeto.name}</p>
    </div>
	
	<div class="col-md-4">
      <p><strong>Username</strong></p>
  	  <p>${objeto.username}</p>
    </div>
	
	<div class="col-md-4">
      <p><strong>Password</strong></p>
  	  <p>${objeto.password}</p>
    </div>
    
    <div class="col-md-4">
      <p><strong>Team</strong></p>
      <c:forEach var="team" items="${objeto.team}">
  	  <p>${team.name}</p>
      </c:foreach>
    </div>
 </div>
 
 <hr />
 <div id="actions" class="row">
   <div class="col-md-12">
     <a href="ControllerServlet?cmd=UserListModelCmd" class="btn btn-primary">Fechar</a>
	 <a href="ControllerServlet?cmd=UserEditModelCmd&id=${objeto.id}" class="btn btn-default">Editar</a>
	 <a href="ControllerServlet?cmd=UserDeleteModelCmd&id=${objeto.id}" class="btn btn-default" data-toggle="modal" data-target="#delete-modal">Excluir</a>
   </div>
 </div>
    </body>
</html>
