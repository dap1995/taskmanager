<%-- 
    Document   : MainMenu
    Created on : 30/10/2015, 12:01:18
    Author     : dap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>Dashboard</title>
        
    </head>
    <body>
        <%@ include file="IMenu.jsp" %>
        <%@ include file="IDatagridTeam.jsp" %>
    </body>
</html>