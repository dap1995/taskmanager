<%-- 
    Document   : MainMenu
    Created on : 30/10/2015, 12:01:18
    Author     : dap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>View</title>
    </head>
    <body>
        <%@ include file="IMenu.jsp" %>
        <%@ include file="IViewTasks.jsp" %>
    </body>
</html>