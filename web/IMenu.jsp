<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assets/js/jquery-2.1.4.js" type="text/javascript"></script>
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('#myTable').dataTable();
            });
        </script>
    </head>
    <body>
        <nav class="navbar-inverse navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><!-- Nome da Empresa via JSP --> TaskManager</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Users <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="ControllerServlet?cmd=UserCreateModelCmd"> Criar </a></li>
                                <li><a href="ControllerServlet?cmd=UserListModelCmd"> Listar </a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Team <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="ControllerServlet?cmd=TeamCreateModelCmd"> Criar </a></li>
                                <li><a href="ControllerServlet?cmd=TeamListModelCmd"> Listar </a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Tasks <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="ControllerServlet?cmd=TasksCreateModelCmd"> Criar </a></li>
                                <li><a href="ControllerServlet?cmd=TasksListModelCmd"> Listar </a></li>
                            </ul>
                        </li>
                    </ul>

                    <form class="navbar-form navbar-left invisible" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-inbox" aria-hidden="true"></span> ${userName} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="LogoutServlet">Logout</a></li>
                                <li role="separator" class="divider"></li>
                                <li><p class="navbar-text">Signed in as ${Name} </p></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </body>
</html>
