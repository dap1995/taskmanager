<%-- 
    Document   : Error
    Created on : 03/12/2015, 19:54:56
    Author     : dap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error Page</title>
    </head>
    <body>
        <%@ include file="IMenu.jsp" %>
         <div id="main" class="container-fluid" style="margin-top: 10px">
             <a id='back' class='btn btn-default btn-sm' href='ControllerServlet?cmd=${back}' >Voltar</a>
             <h2>Ocorreu o seguinte erro:<p></p> ${error}?</h2>
                    <p></p>
        </div> <!-- /#main -->
    </body>
</html>
