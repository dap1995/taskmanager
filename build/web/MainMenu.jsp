<%-- 
    Document   : MainMenu
    Created on : 30/10/2015, 12:01:18
    Author     : dap
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assets/js/jquery-2.1.4.js" type="text/javascript"></script>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <title>Dashboard</title>
        <%!
        Long userID;
        String userName;
        String Name;
        %>
        <%
        
        if (session != null){
            if ((session.getAttribute("userId") != null) && (session.getAttribute("userName") != null)){            
                userID = (Long) session.getAttribute("userId");
                userName = (String) session.getAttribute("userName");
                Name = (String) session.getAttribute("Name");
            } else {
                session.invalidate();
                response.sendRedirect("Login.html");
            }
        } else {
            response.sendRedirect("Login.html");
        }
        %>
    </head>
    <body>
        <%@ include file="IMenu.jsp" %>
        <%@ include file="IDashboard.jsp" %>
    </body>
</html>